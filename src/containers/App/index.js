import React, { Component } from 'react'

import axios from 'axios';

function Header() {
	return (
		<section className="hero">
		  <div className="hero-body">
		    <div className="container">
		      <h1 className="title">
		        Surveys!
		      </h1>
		      <h2 className="subtitle">
		        For you! Here and now!
		      </h2>
		    </div>
		  </div>
		</section>
	)
}


class Survey extends Component {
	constructor(props) {
		super(props);
		this.startCreate = this.startCreate.bind(this);
		this.finishCreate = this.finishCreate.bind(this);
		this.state = {
			isCreating: false,
			surveyLink: ''
		}
	}
	startCreate(e) {
		console.log('ey')
		e.preventDefault();
		this.setState({isCreating: true})
	}
	finishCreate(questions, finalQ) {
		let ajaxObj = {questions: questions}
		if (finalQ) ajaxObj.questions.push(finalQ)
		console.log('ok job')
		console.log('AJAX http://', ajaxObj)
		let creationUrl = '/api/survey'
		axios.post(creationUrl, ajaxObj)
			.then(res => {
				console.log('uh', res)
				if (res.data.status === 'success') {
					this.setState({surveyLink: '/survey/'+res.data.id})
				}
			})
	}
	render() {
		return(
			<div className="container">
				<Header />
				{
					this.state.isCreating ? <SurveyCreator finishCreate={this.finishCreate} isFinished={!!this.state.surveyLink} /> 
					: <a href="#" className="button is-large" onClick={this.startCreate}>Create a survey!</a>
				}

				{
					this.state.surveyLink && 
					<div>
					Here's your survey link: <a href={this.state.surveyLink}>Survey!</a>
					</div>
				}
				
			</div>
		)
	}
}

export default Survey


class SurveyCreator extends Component {
	constructor(props) {
		super(props);
		this.finishQuestion = this.finishQuestion.bind(this);
		this.finishSurvey = this.finishSurvey.bind(this);
		this.state = {
			questions: []
		}
	}
	finishQuestion(q, ans) {
		this.setState({
			questions: [...this.state.questions, {question: q, answers: ans}]
		})
		console.log('oh done witha  q');
	}
	finishSurvey(finalQ) {
		this.props.finishCreate(this.state.questions, finalQ)
	}
	render() {
		console.log('SURV STATE: ', this.state)
		let questions = []
		let numQuestions = this.state.questions.length;
		if (this.props.isFinished) numQuestions--;
		for(var i =0; i <= numQuestions; i++) {
			console.log('idgi', i)
			questions.push(<Question handleQuestionFinish={this.finishQuestion} handleSurveyFinish={this.finishSurvey} key={'q-'+i} />)
		}
		return (
			<div>
				{
					questions
				}
				
			</div>
		)
	}
}


class Question extends Component {
	constructor(props) {
		super(props);
		this.handleQInput = this.handleQInput.bind(this);
		this.handleAnswerInput = this.handleAnswerInput.bind(this);
		this.handleDoneQuestion = this.handleDoneQuestion.bind(this);
		this.state = {
			question: '',
			qType: 'open',
			answers: ['', ''],
			isFinished: false
		}
	}
	handleQInput(e) {
		this.setState({question: e.target.value})
	}
	handleAnswerInput(val, modifiedIndex) {
		this.setState({
			answers: this.state.answers.map((ans, ind) => {
				if (ind !== modifiedIndex) {
					return ans
				} else {
					return val
				}
			})
		})
	}
	addAnAnswer() {
		this.setState({
			answers: [...this.state.answers, '']
		})
	}
	handleDoneQuestion() {
		if (this.state.question && this.state.answers[0] && this.state.answers[1]) {
			this.setState({isFinished: true})
			this.props.handleQuestionFinish(this.state.question, this.state.answers)
		}
	}
	doneCreating() {
		let finalQ = {};
		if (this.state.question && this.state.answers[0] && this.state.answers[1]) {
			//this.props.handleQuestionFinish(this.state.question, this.state.answers)
			finalQ = {question: this.state.question, answers: this.state.answers}
		}
		this.setState({isFinished: true})
		this.props.handleSurveyFinish(finalQ)
	}
	render() {
		return (
			<div>
				{
					!this.state.isFinished ?
					<div>
					<QuestionInput questionChangeHandler={this.handleQInput} />

					<Answers answers={this.state.answers} answerChangeHandler={this.handleAnswerInput} />
					<div>
						<a href="#" className="button is-medium" onClick={(e) => {e.preventDefault(); this.addAnAnswer()}}>+</a>
						<NextQuestion nextHandler={this.handleDoneQuestion} />
						<a href="#" className="button is-medium is-primary" onClick={(e) => {e.preventDefault(); this.doneCreating()}}>Done creating</a>
					</div>
					</div>
					: <div className="finished-question">{this.state.question}</div>
				}
				
			</div>
		)
	}
}

class Answers extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { answers, answerChangeHandler } = this.props;
		return (
			<div>
			{
				answers.map((e, i) => {
					return (
						<input className="input"
						type="text"
						placeholder={`Answer #${i+1}`}
						onChange={(e) => answerChangeHandler(e.target.value, i)} 
						key={'ans-'+i} />
					)
				})
			}
			</div>
		)
	}
}

function QuestionInput({question, questionChangeHandler}) {
	return (
		<div className="field">
		  <label className="label">Make a question:</label>
		  <p className="control">
		    <input className="input" type="text" placeholder="What's your question" onChange={(e) => questionChangeHandler(e)} />
		  </p>
		</div>
	)
}

function NextQuestion({nextHandler}) {
	return (
		<a href="#" className="button is-medium" onClick={(e)=>{e.preventDefault; nextHandler()}}>Next question</a>
	)
}

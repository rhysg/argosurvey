import React, { Component } from 'react'
import axios from 'axios'

class Survey extends Component {
	constructor(props) {
		super(props);
		this.state = {
			questions: [],
			answers: {},
			submitted: false
		}
	}
	componentDidMount() {

		let id = location.href.substr(location.href.lastIndexOf('/') + 1)
		console.log('id', id)
		axios.get(`/api/survey/${id}`)
			.then(res => {
				console.log(res)
				this.setState({questions: res.data[0].questions, id: res.data[0].id})
			})
	}

	submitResponse() {
		console.log('submittin', this.state.answers)
		axios.post(`/api/survey/${this.state.id}/response`, {
			answers: this.state.answers
		})
		this.setState({submitted: true})
	}

	handleSelection(question, answer) {
		let newanswers = {...this.state.answers}
		newanswers[question] = answer
		this.setState({answers: newanswers})
	}

	render() {
		return (
			<div className="container">
			{ this.state.submitted ? <div>Thanks</div> :

					this.state.questions ? 
					<div>
					{this.state.questions.map((q, i) => {
						console.log('uh', q)
						return (
							<section className="hero is-primary" key={q.question+i} >
							  <div className="hero-body">
							    <div className="container">
							      <h1 className="title">
							        {q.question}
							      </h1>
							      <h2 className="subtitle">
							        
							      </h2>
							      <div>
							      	{
							      		q.answers.map((ans, i) => {
							      			return (
							      				<div key={ans+i}>
							      				<input type="radio" name={q.question} id={'ans'+ans+i} onChange={(e) => {this.handleSelection(q.question, ans)}} />
							      				<label htmlFor={'ans'+ans+i} className="radio">{ans}</label>
							      				</div>
							      			)
							      		})
							      	}
							      </div>
							    </div>
							  </div>

							</section>
						)
					})}
					<a href="#" className="button is-medium is-primary" onClick={(e) => {e.preventDefault(); this.submitResponse()}}>Submit response</a>
					</div> : null
					
				
			}
			
			
			</div>
		)
	}
}

export default Survey
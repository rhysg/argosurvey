import React, { Component } from 'react';
import { render } from 'react-dom'

import App from './containers/App'


var rootElement = document.getElementById('root')
if (rootElement) {
	render(
	    <App/>,
	  	rootElement
	)

	// Hot Module Replacement API
	if (module.hot) {
	  module.hot.accept('./containers/App', () => {
	    const NextApp = require('./containers/App').default;
	    render(
	      <NextApp/>,
	      document.getElementById('root')
	    );
	  });
	}
}



import TakeSurvey from './containers/TakeSurvey'

var surveyElement = document.getElementById('survey');
if (surveyElement) {
	render(
		<TakeSurvey />,
		surveyElement)

	if (module.hot) {
	  module.hot.accept('./containers/TakeSurvey', () => {
	    const NextApp = require('./containers/TakeSurvey').default;
	    render(
	      <NextApp/>,
	      document.getElementById('survey')
	    );
	  });
	}
}
var r = require('rethinkdb');
var assert = require('assert');

var host = process.env.RDB_HOST || 'localhost';
var port = process.env.RDB_PORT || 28015;

var password = process.env.RDB_PASS || 'password';

var dbConfig = {
  //host: process.env.RDB_HOST || 'localhost',
  host: host,
  port: parseInt(process.env.RDB_PORT) || 28015,
  db  : process.env.RDB_DB || 'argosurvey',
  tables: {
    'surveys': 'id',
    'responses': 'id'
  }
};

module.exports.setup = function() {
	r.connect({host: dbConfig.host, port: dbConfig.port, user:'argosurvey', password: password}, function(err, conn) {
		if (err) console.log('rdb connection error', err);
		//
		r.dbCreate(dbConfig.db).run(conn, function(err, result) {
			if (err) {
				console.log('db already exists');
			} else {
				console.log('db created');
			}
			for(var tbl in dbConfig.tables) {
		        (function (tableName) {
		          r.db(dbConfig.db).tableCreate(tableName, {primaryKey: dbConfig.tables[tbl]}).run(conn, function(err, result) {
		            if(err) {
		              console.log('error creating table: ', tableName)
		            }
		            else {
		              console.log('table created: ', tableName)
		            }
		          });
				})(tbl);
			}
		})
	})
}

module.exports.addSurvey = function(questions, callback) {
	onConnect(function(err, conn) {
		r.db('argosurvey').table('surveys').insert({questions: questions}).run(conn, function(err, result) {
			if (err) {
				console.log('there was an error inserting the questions', err);
			} else {
				console.log('there were ' + result.inserted + ' documents inserted', result);
				console.log('there were ' + result.errors + ' errors');
				if (result.errors) {
					callback(result.errors)

				} else {
					callback(null, result)
				}
				conn.close();
				
			}
			
		});
	});
}

module.exports.addResponse = function(surveyId, answers, callback) {
	onConnect(function(err, conn) {
		r.db('argosurvey').table('responses').insert({answers: answers, surveyId: surveyId}).run(conn, function(err, result) {
			if (err) {
				console.log('there was an error inserting the questions', err);
			} else {
				console.log('there were ' + result.inserted + ' documents inserted', result);
				console.log('there were ' + result.errors + ' errors');
				if (result.errors) {
					callback(result.errors)

				} else {
					callback(null, result)
				}
				conn.close();
				
			}
			
		});
	});
}

module.exports.getSurveyById = function(surveyId, callback) {
	onConnect(function(err, conn) {
		r.db('argosurvey').table('surveys').filter({id: surveyId}).run(conn, function(err, cursor) {
			if (err) {
				console.log('error gettin this', err);
				callback(null, []);
			} else {
				console.log('ok gotten', cursor);
				cursor.toArray(function(err, results) {
		            if (err) {
		                callback(err)
		            }
		            else {
		                console.log('results', results);
		                callback(null, results)
		            }
		        })
			}
			conn.close();
		})
	})
}


//a wrapper for rethinkdb's r.connect
//lifted from rdb's codestuff and a good idea probably!!
// * to keep the configuration details in a single function
// * and fail fast in case of a connection error.
function onConnect(callback) {
	r.connect({host: host, port: port, user:'argosurvey', password: password}, function(err, conn) {
		assert.ok(err === null, err);
		conn['_id'] = Math.floor(Math.random()*10001);
		callback(err, conn);
	})
}
var express = require('express');
var router = express.Router();

var db = require('../db')
const uuidv4 = require('uuid/v4');


router.post('/survey', function(req, res) {
	console.log('posted! ', req.body)
	db.addSurvey(req.body.questions, function(err, result) {
		console.log(result)
		res.json({status: 'success', id: result.generated_keys[0]})
	})
	
})

router.post('/survey/:surveyId/response', function(req, res) {
	console.log('posted! ', req.body)
	db.addResponse(req.params.surveyId, req.body.answers, function(err, result) {
		console.log(result)
		res.json({status: 'success', id: result.generated_keys[0]})
	})
	
})

router.get('/survey/:surveyId', function(req, res) {
	console.log('gettin', req.params.surveyId)
	db.getSurveyById(req.params.surveyId, function(err, result) {
		console.log(result);
		res.json(result)
	})
	
})

module.exports = router;